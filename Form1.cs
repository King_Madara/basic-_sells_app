﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace App_de_Wallmart
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Es bueno utilizar una estuctua de contol como trycatch para que, si se comete un error,
            //este esté controlado y no dañe al prorama o la base de datos
            try
            {
                //dentro del paréntesis esta la dirección de la que proviene las variables de conexión
                SqlConnection Coneccion = new SqlConnection("Data Source=.;Initial Catalog=Administración;Integrated Security=True");
                Coneccion.Open(); //Abre la conexión con la database
                MessageBox.Show("Se ha conectado");
                //abre una nueva ventana que dice un mensaje

            }
            catch(Exception error)
            {
                MessageBox.Show("Ha ocurrido un error" + error.Message);
            }
        }
    }
}
